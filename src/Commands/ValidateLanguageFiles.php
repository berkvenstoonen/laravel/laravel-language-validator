<?php

namespace BeTo\LaravelLanguageValidator\Commands;

use BeTo\Laravel\Exceptions\ProgrammingException;
use Illuminate\Console\Command;
use Webmozart\Assert\Assert;

final class ValidateLanguageFiles extends Command
{
    protected     $signature   = 'validate:language-files';
    protected     $description = 'This command will check the entries of all the language files to see if there are discrepancies in any of them';
    private array $knownDuplicates;
    private bool  $success     = true;

    /**
     * @throws ProgrammingException
     */
    public function handle(): int
    {
        $this->validateLanguageFileExistence();
        if (!$this->success) {
            return 1;
        }

        $knownDuplicatesPath   = $this->getLanguagesPath() . DIRECTORY_SEPARATOR . 'known-duplicates.php';
        $this->knownDuplicates = file_exists($knownDuplicatesPath) ? include $knownDuplicatesPath : [];

        $items = $this->scandir($this->getLanguagePath($this->getPrimaryLanguage()));
        Assert::isArray($items);
        $this->loopItems($items);

        if (!$this->success) {
            return 1;
        }
        $this->output->writeln('<info>No inconsistencies found</info>');
        return 0;
    }

    /**
     * @param array<string> $items
     * @throws ProgrammingException
     */
    private function loopItems(array $items, string $prefix = ''): void
    {
        foreach ($items as $item) {
            if (str_starts_with($item, '.')) {
                continue;
            }
            $item = $prefix . $item;
            if (is_dir($this->getLanguageFile($this->getPrimaryLanguage(), $item))) {
                $this->loopItems($this->scandir($this->getLanguageFile($this->getPrimaryLanguage(), $item)), $item . DIRECTORY_SEPARATOR);
            } else {
                $this->validateLanguageFiles($item);
            }
        }
    }

    /**
     * @throws ProgrammingException
     */
    private function validateLanguageFileExistence(): void
    {
        $headers = [];
        $rows    = [];
        foreach ($this->getAllLanguages() as $language) {
            $headers[] = $language;
            foreach ($this->scandir($this->getLanguagePath($language)) as $file) {
                if (str_starts_with($file, '.')) {
                    continue;
                }
                $rows[$file][$language] = $file;
            }
        }
        $this->processMissing($headers, $rows);
    }

    /**
     * @param array<string> $headers
     * @param array<array<string>> $rows
     * @throws ProgrammingException
     */
    private function processMissing(array $headers, array $rows, string $label = null): void
    {
        $showTable = false;
        ksort($rows);
        $expectedRowItemsCount = count($headers);
        $rowsToPrint           = [];
        foreach ($rows as $row) {
            if (count($row) !== $expectedRowItemsCount) {
                $showTable     = true;
                $this->success = false;
                $row           += $this->getMissingMessages();
                uksort($row, $this->sortRow(...));
                $rowsToPrint[] = $row;
            }
        }
        if ($showTable) {
            if ($label !== null) {
                $this->output->writeln($label);
            }
            $this->output->table($headers, $rowsToPrint);
        }
    }

    /**
     * @param array<string> $headers
     * @param array<array<string>> $rows
     */
    private function processDuplicates(array $headers, array $rows, string $label): void
    {
        $showTable = false;
        ksort($rows);
        $rowsToPrint = [];
        foreach ($rows as $key => &$row) {
            $checkedValues = [];
            $duplicates    = [];
            foreach ($row as $language => &$value) {
                if (in_array(strtolower($value), $this->knownDuplicates)) {
                    continue;
                }
                if (!array_key_exists($value, $checkedValues)) {
                    $checkedValues[$value] = [$language];
                } else {
                    if (!array_key_exists($value, $duplicates)) {
                        $showTable          = true;
                        $duplicates[$value] = $checkedValues[$value];
                    }
                    $duplicates[$value][] = $language;
                }
                $value = $key;
            }
            foreach ($duplicates as $languages) {
                $firstLanguage = array_shift($languages);
                foreach ($languages as $language) {
                    $row[$language] = '<error>duplicate of <comment>' . $firstLanguage . '</comment></error>';
                }
            }
            if (!empty($duplicates)) {
                $rowsToPrint[] = $row;
            }
        }
        if ($showTable) {
            $this->output->writeln($label);
            $this->output->table($headers, $rowsToPrint);
        }
    }

    /**
     * @throws ProgrammingException
     */
    private function validateLanguageFiles(string $file): void
    {
        $headers = [];
        $keys    = [];
        $values  = [];
        foreach ($this->getAllLanguages() as $language) {
            $headers[] = $language;
            foreach ($this->getKeysForArray($this->getLanguageFileContents($language, $file)) as $key) {
                $keys[$key][$language] = $key;
            }
            foreach ($this->getValuesForArray($this->getLanguageFileContents($language, $file)) as $key => $value) {
                $values[$key][$language] = $value;
            }
        }
        usort($headers, $this->sortRow(...));
        $this->processMissing($headers, $keys, $file);
        $headers[0] = 'key';
        $this->processDuplicates($headers, $values, $file);
    }

    /**
     * @param array<array<string>|string> $items
     * @param string $prefix
     * @return array<string>
     */
    private function getKeysForArray(array $items, string $prefix = ''): array
    {
        $keys = [];
        foreach ($items as $key => $item) {
            $keys[] = $prefix . $key;
            if (is_array($item)) {
                $keys = array_merge($keys, $this->getKeysForArray($item, $key . '/'));
            }
        }
        return $keys;
    }

    /**
     * @param array<array<string>|string> $items
     * @param string $prefix
     * @return array<string>
     */
    private function getValuesForArray(array $items, string $prefix = ''): array
    {
        $values = [];
        foreach ($items as $key => $value) {
            if (is_array($value)) {
                $values = array_merge($values, $this->getValuesForArray($value, $key . '/'));
            } else {
                $values[$prefix . $key] = $value;
            }
        }
        return $values;
    }

    #region Helper Functions
    private string $relativeLanguagesPath;

    private function getRelativeLanguagesPath(): string
    {
        if (!isset($this->relativeLanguagesPath)) {
            if (is_dir(base_path() . DIRECTORY_SEPARATOR . 'lang')) {
                $this->relativeLanguagesPath = 'lang';
            } else {
                $this->relativeLanguagesPath = 'resources' . DIRECTORY_SEPARATOR . 'lang';
            }
        }
        return $this->relativeLanguagesPath;
    }

    private string $languagesPath;

    private function getLanguagesPath(): string
    {
        if (!isset($this->languagesPath)) {
            $this->languagesPath = base_path() . DIRECTORY_SEPARATOR . $this->getRelativeLanguagesPath();
        }
        return $this->languagesPath;
    }

    private function getLanguagePath(string $language): string
    {
        return $this->getLanguagesPath() . DIRECTORY_SEPARATOR . $language;
    }

    private function getLanguageFile(string $language, string $file): string
    {
        return $this->getLanguagePath($language) . DIRECTORY_SEPARATOR . $file;
    }

    /**
     * @return array<array<string>|string>
     */
    private function getLanguageFileContents(string $language, string $file): array
    {
        return include $this->getLanguageFile($language, $file);
    }

    private string $primaryLanguage;

    private function getPrimaryLanguage(): string
    {
        if (!isset($this->primaryLanguage)) {
            $primaryLanguage = config('app.fallback_locale');
            Assert::string($primaryLanguage);
            $this->primaryLanguage = $primaryLanguage;
        }
        return $this->primaryLanguage;
    }

    /** @var array<string> */
    private array $otherLanguages;

    /**
     * @return array<string>
     * @throws ProgrammingException
     */
    private function getOtherLanguages(): array
    {
        if (!isset($this->otherLanguages)) {
            $this->otherLanguages = array_filter($this->scandir($this->getLanguagesPath()), function ($language) {
                return is_dir($this->getLanguagesPath() . DIRECTORY_SEPARATOR . $language) && !str_starts_with($language, '.') && $language !== $this->getPrimaryLanguage();
            });
        }
        return $this->otherLanguages;
    }

    /** @var array<string> */
    private array $allLanguages;

    /**
     * @return array<string>
     * @throws ProgrammingException
     */
    private function getAllLanguages(): array
    {
        if (!isset($this->allLanguages)) {
            $this->allLanguages = $this->getOtherLanguages();
            array_unshift($this->allLanguages, $this->getPrimaryLanguage());
        }
        return $this->allLanguages;
    }

    /** @var array<string, string> */
    private array $missingMessages;

    /**
     * @return array<string, string>
     * @throws ProgrammingException
     */
    private function getMissingMessages(): array
    {
        if (!isset($this->missingMessages)) {
            $this->missingMessages = array_fill_keys($this->getAllLanguages(), '<error>---missing---</error>');
        }
        return $this->missingMessages;
    }

    private function sortRow(string $a, string $b): int
    {
        if ($a === $this->getPrimaryLanguage()) {
            return -1;
        }
        if ($b === $this->getPrimaryLanguage()) {
            return 1;
        }
        return $a <=> $b;
    }

    /**
     * @return array<string>
     * @throws ProgrammingException
     */
    private function scandir(string $path): array
    {
        $items = scandir($path);
        if ($items === false) {
            throw new ProgrammingException('Could not scandir '.$path);
        }
        return $items;
    }
    #endregion
}
