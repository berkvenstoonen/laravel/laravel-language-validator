<?php

namespace BeTo\LaravelLanguageValidator;

use BeTo\LaravelLanguageValidator\Commands\ValidateLanguageFiles;
use Illuminate\Support\ServiceProvider;

final class LanguageValidationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            ValidateLanguageFiles::class
        ]);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
