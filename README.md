<p align="center">
    <img src="https://be-to.nl/images/round.png" alt="BeTo Logo" style="background-color: transparent"/>
</p>

# Language File Validator for Laravel
![Version](https://img.shields.io/packagist/v/beto/laravel-language-validator?style=for-the-badge&label=Version&link=https%3A%2F%2Fpackagist.org%2Fpackages%2Fbeto%2Flaravel-language-validator)
![PHP Version](https://img.shields.io/packagist/php-v/beto/laravel-language-validator?style=for-the-badge&label=PHP&link=https%3A%2F%2Fwww.php.net%2Freleases%2F8.2%2Fen.php)
![Laravel Version](https://img.shields.io/packagist/dependency-v/beto/laravel-language-validator/laravel%2Fframework?style=for-the-badge&label=Laravel&link=https%3A%2F%2Flaravel.com%2F)
![Contributions Welcome](https://img.shields.io/badge/contributions-welcome-green.svg?style=for-the-badge&link=https%3A%2F%2Fgitlab.com%2Fberkvenstoonen%2Flaravel%2Fplugins%2Flanguage-validator)
